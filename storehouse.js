let componentTypes = {
  "electronic": "electronic",
  "mechanical": "mechanical"
}

let storehouse = {
  "Andrea": {
    "address": "Via R. Sassi, 77 - 60044 Fabriano"
  },
  "Enrico": {
    "address": "Via P. Nenni - 60044 Fabriano"
  }
}




let part = {
  "jack-neu-np2x": {
    "type": "electronic",
    "type": "audio-jack",
    "description": "1/4 professional phone plug, 2 pole, nickel contacts, nickel shell",
    "tags": ["jack", "mono", "1/4"],
    "manifacturer": "Neutrik",
    "partno": "NP2X",
    "url": "https://www.neutrik.com/en/product/np2x",
    "imageUrl": "https://www.neutrik.com/uploads/media/500x/01/681-np2x.jpg?v=1-0"
  }
}

let inhouse = {
  "jack-neu-np2x": {
    "Andrea": {
      "qty": 2
    },
    "Enrico": {
      "qty": 0
    }
  }
}

let supplier = {
  "Amazon-it": {
    url: "http://www.amazon.com"
  },
  "Tube-Town": {
    url: "http://tube-town.net/ttstore/"
  },
  "Conrad": {
    url: "http://www.conrad.it"
  }
}



let buyList = {
  "jack-neu-np2x": [{
    "date": "",
    "time": "",
    "from": "Amazon-it",
    "receipt_file": "",
    "unit_price": 2.85,
    "purchase_id": "abc123"
  }]
}
