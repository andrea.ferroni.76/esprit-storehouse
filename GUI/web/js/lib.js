showHideSubType = function (selector) {
  if (selector.name === "tipo_componente") {
    if (selector.options[selector.selectedIndex].value === "mechanical") {
      document.getElementById("selettore-componenti-meccaniche").classList.remove("is-hidden")
      document.getElementById("selettore-componenti-elettroniche").classList.add("is-hidden")
    }
    if (selector.options[selector.selectedIndex].value === "electronical") {
      document.getElementById("selettore-componenti-meccaniche").classList.add("is-hidden")
      document.getElementById("selettore-componenti-elettroniche").classList.remove("is-hidden")
    }
  }
}

setGUI = function () {
  showHideSubType(document.getElementsByName("tipo_componente")[0])
}