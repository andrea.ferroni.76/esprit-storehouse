Esprit Storehouse
=================

This package is a very simple internal storage manager for a little company.

Components are:
* a web GUI built with wonderful [bulma CSS framework](https://bulma.io/)
* a server in nodejs/express


Make it work
------------

Actually is meant for personal local installation and usage, nothing else.

Launch with: `NODE_ENV="production" node ./server/server.js`
